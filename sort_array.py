# -*- coding: utf-8 -*-
import insertion_sort as isort

def toInt(element):
  try:
    int(element)
    return True, int(element) # Если успешно преобразовали в число возвращаем True и само число
  except ValueError:
    return False, element # Иначе возвращаем False и элемент массива, который не смогли преобразовать в число

def main():
  nums = []
  i = 0
  
  while (i < 20):
    inp = input("Введите %d-е число через enter\n" % (i+1))
    if inp == '': # Нажали второй раз Enter - закончили ввод чисел
      break
    res, value = toInt(inp)
    if (res != False):
      nums.append(value)
    else:
      print('Введите целое число, а не', value)
      return False
    i+=1

  print("Массив до сортировки:")
  print(nums)
  print("Массив после сортировки:")
  print(isort.insertion_sort(nums))







if __name__ == '__main__':
  main()