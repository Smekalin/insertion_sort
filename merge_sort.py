def merge_sort(A):
  if len(A) <= 1:
    return A
  middle = int(len(A) / 2)
  left_part = merge_sort(A[:middle])
  right_part = merge_sort(A[middle:])
  return merge(left_part, right_part)


def merge(left, right):
  res = []

  while len(left) > 0 and len(right) > 0:
    if left[0] < right[0]:
      res.append(left[0])
      left = left[1:]
    else:
      res.append(right[0])
      right = right[1:]
  if len(left) > 0:
    res.extend(left)
  if len(right) > 0:
    res.extend(right)
  return res