import unittest
import insertion_sort as isort
import merge_sort as msort

class TestInsertionSort(unittest.TestCase):

  def test_sort_1(self):
      self.assertEqual(isort.insertion_sort([5,4,3,2,1]), [1,2,3,4,5])

  def test_sort_2(self):
      self.assertEqual(isort.insertion_sort([15,-44,3453,33332,-2342341, -0, 0, 1002]), [-2342341, -44, 0, 0, 15, 1002, 3453, 33332])

  def test_sort_3(self):
      self.assertEqual(isort.insertion_sort([1,2,3,4,5]), [1,2,3,4,5])

class TestMergeSort(unittest.TestCase):

  def test_sort_1(self):
      self.assertEqual(msort.merge_sort([5,4,3,2,1]), [1,2,3,4,5])

  def test_sort_2(self):
      self.assertEqual(msort.merge_sort([6,2,4,5,3,1,2,1]), [1,1,2,2,3,4,5,6])

  def test_sort_3(self):
      self.assertEqual(msort.merge_sort([15,-44,3453,33332,-2342341, -0, 0, 1002]), [-2342341, -44, 0, 0, 15, 1002, 3453, 33332])

  def test_sort_4(self):
      self.assertEqual(msort.merge_sort([1,2,3,4,5]), [1,2,3,4,5])


if __name__ == '__main__':
  unittest.main()