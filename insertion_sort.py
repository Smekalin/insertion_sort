def insertion_sort(nums): # Сортировка вставками
  sort_array = nums
  for i, _ in enumerate(nums):
    j = i;
    while ((j > 0) and (nums[j] < nums[j-1])):
      nums[j], nums[j-1] = nums[j-1], nums[j]
      j -= 1
  return(sort_array)
